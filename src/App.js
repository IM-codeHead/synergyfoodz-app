import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { Switch, Route } from 'react-router-dom';
import checkoutPage from './containers/checkoutPage';
import orderPage from './containers/orderPage';
import loginPage from './containers/loginPage';
import Loader from './components/Loader';
import { database } from './firebase';
import * as baseActions from "./actions";

const loader = () => <Loader withText={false} />;

class App extends React.Component {
  componentDidMount() {
    database.ref('/locations').on('value', snapshot => {
      const state = snapshot.val();
      this.props.actions.setLocations(state);
      this.props.actions.setLoading(false);
    });
  }

  render() {
    return (
      <div className="App">
        <Switch>
          <Route
            exact
            key={'app/loginPage'}
            path={'/'}
            component={this.props.baseReducer.loading ? loader : loginPage}
          />
          <Route
            exact
            key={'app/checkoutPage'}
            path={'/checkoutPage'}
            component={this.props.baseReducer.loading ? loader : checkoutPage}
          />
          <Route
            exact
            key={'app/placeOrderPage'}
            path={'/order'}
            component={orderPage}
          />
        </Switch>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

function mapDispatchToProps(dispatch) {
  const actions = bindActionCreators({ ...baseActions }, dispatch);
  return {
    actions
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
