import { SET_STORE, SET_URLS, SET_LOADING, ADD_REMOVE_ITEM, UPDATE_QUANTITY, CLEAR_SELECTIONS, SET_LOCATION_DATA, SET_LOCATIONS } from './constants';

export const setStore = data => ({ type: SET_STORE, data });

export const setUrls = data => ({ type: SET_URLS, data });

export const setLoading = data => ({ type: SET_LOADING, data });

export const addRemoveItem = data => ({ type: ADD_REMOVE_ITEM, data });

export const updateQuantity = data => ({ type: UPDATE_QUANTITY, data });

export const clearSelections = data => ({ type: CLEAR_SELECTIONS });

export const setLocationData = data => ({ type: SET_LOCATION_DATA, data });

export const setLocations = data => ({ type: SET_LOCATIONS, data });
