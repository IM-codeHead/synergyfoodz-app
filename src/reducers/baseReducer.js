import { SET_STORE, SET_URLS, SET_LOADING, ADD_REMOVE_ITEM, UPDATE_QUANTITY, CLEAR_SELECTIONS, SET_LOCATION_DATA, SET_LOCATIONS } from "../constants";

const initialState = {
  currentDayData: {},
  itemDetails: {},
  imagesUrls: {},
  loading: true,
  currentLocation: {},
  locations: [],
};

const baseReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_STORE: {
      return {...state, ...action.data };
    }
    case SET_URLS: {
      return {...state, ...action.data };
    }
    case SET_LOADING: {
      return {...state, loading: action.data };
    }
    case ADD_REMOVE_ITEM: {
      const itemId = action.data.id;
      const currentDayData = state.currentDayData;
      const itemIndex = currentDayData.findIndex(obj => obj.id === itemId);
      currentDayData[itemIndex].checked = action.data.checked;
      return {...state, currentDayData };
    }
    case UPDATE_QUANTITY: {
      const itemId = action.data.id;
      const currentDayData = state.currentDayData;
      const itemIndex = currentDayData.findIndex(obj => obj.id === itemId);
      currentDayData[itemIndex].quantity = action.data.quantity;
      return {...state, currentDayData };
    }
    case CLEAR_SELECTIONS: {
      const currentDayData = state.currentDayData.map(obj => { return { ...obj, checked: false }; });
      return {...state, currentDayData };
    }
    case SET_LOCATIONS: {
      return {...state, locations: action.data };
    }
    case SET_LOCATION_DATA: {
      const id = action.data;
      const index = state.locations.map(el => el.locationId).indexOf(id);
      return {...state, currentLocation: state.locations[index] };
    }
    default: {
      return state;
    }
  }
};

export default baseReducer;
