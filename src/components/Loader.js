import React from 'react';

const Loader = props => (
<div className="loader">
  <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
  {props.withText && <div className="loader-text">{props.text}</div>}
</div>);

export default Loader;
