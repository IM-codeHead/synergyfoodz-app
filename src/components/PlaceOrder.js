import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { database } from '../firebase';
import * as itemActions from "../actions";

class PlaceOrderBtn extends Component {
  state = { mobileNo: '', showModal: false }

  onClick = () => {
    this.setState({ showModal: true });
  }

  onSubmit = async () => {
    const { currentDayData, currentLocation } = this.props.baseReducer;
    const rootRef = database.ref();
    const storesRef = rootRef.child(`/bills/${currentLocation.locationId}`);
    const currentData = currentDayData.filter(itemData => itemData.checked);
    const total = currentData.map(obj => obj.price * obj.quantity).reduce((acc, red) => {
      return acc + red;
    });

    const snapshot = await storesRef.child('count').once('value');
    const count = (snapshot && snapshot.val()) || 0;
    const dateObj = new Date();
    const timeStamp = dateObj.toLocaleString();
    const billObj = {
      items: currentData.map(({ name, quantity, price, id }) => { return { name, quantity, price, id }; }),
      total,
      timeStamp,
      billNo: count + 1,
      mobileNo: this.state.mobileNo,
      locationId: currentLocation.locationId,
      locationName: currentLocation.locationName,
    };

    const newBillRef = storesRef.child(`${dateObj.toDateString()}, ${dateObj.toLocaleTimeString()}`);
    await newBillRef.set(billObj);
    storesRef.child('count').set(count + 1);
    console.log(`added bill ${timeStamp} with bill number ${count + 1} to database`);

    await  this.props.actions.clearSelections();
    await  this.props.history.push('/checkoutPage');
  }

  render() {
    return (
      <React.Fragment>
        <button
          className={`place-order-btn`}
          onClick={this.onClick}
          disabled={this.props.disable}
          >
          Place order
        </button>
        {this.state.showModal &&
          <div className="modal">
          <button
            className="modal-background"
            onClick={e => {
              e.preventDefault();
              e.stopPropagation();
            }}/>
          <input
            type="number"
            placeholder="Enter Mobile Number"
            min="0"
            onChange={e => {
               if (e.target.value.length <= 10) {
                 this.setState({ mobileNo: e.target.value });
               }
             }}
             onKeyDown={e => {
               if (
                 e.which === 38 ||
                 e.which === 40 ||
                 e.which === 69 ||
                 e.which === 189
               ) {
                 e.preventDefault();
               }
             }}
            value={this.state.mobileNo}
            />
          <div className="btn-panel">
            <button
              onClick={this.onSubmit}
              >
              Confirm
            </button>
            <button
              onClick={() => {
                this.setState({ showModal: false, mobileNo: '' });
              }}
              >
              Cancel
            </button>
          </div>
        </div>}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

function mapDispatchToProps(dispatch) {
  const actions = bindActionCreators({ ...itemActions }, dispatch);
  return {
    actions
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlaceOrderBtn);
