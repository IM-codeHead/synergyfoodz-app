import React from 'react';

const PasswordField = props => (
<div className="password-field">
  <input type="password" onChange={props.onChange}/>
</div>);

export default PasswordField;
