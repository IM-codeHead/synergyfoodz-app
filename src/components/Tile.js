import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import * as itemActions from "../actions";

class Tile extends Component {
  constructor(props) {
    super(props);
    const currentDayData = props.baseReducer.currentDayData;
    const index = currentDayData.findIndex(obj => obj.id === props.id);
    this.state = { checked: currentDayData[index].checked };
  }

  onClick = () => {
    this.props.actions.addRemoveItem({ id: this.props.id, checked: !this.state.checked });
    this.setState({ checked: !this.state.checked });
  }

  render() {
    const { imageSrc, name, price } = this.props;
    return (
      <button
        className={`tile-btn ${this.state.checked ? 'checked' : ''}`}
        onClick={this.onClick}
        >
        <img src={imageSrc} alt={name} />
        <span>{name}</span>
        <div> &#8377; {price} </div>
      </button>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

function mapDispatchToProps(dispatch) {
  const actions = bindActionCreators({ ...itemActions }, dispatch);
  return {
    actions
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Tile);
