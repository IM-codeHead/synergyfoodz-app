import React, {Component} from 'react';
// import {init as firebaseInit} from '../javascripts/firebase';
// import configureStore from './configureStore';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import Tile from './Tile';
import * as itemActions from "../actions";
import Loader from './Loader';

class ItemsGrid extends Component {
  state = { loadedCurrentData: [], loading: true, };

  itemGrid() {
    const { imagesUrls } = this.props.baseReducer;
    const tiles = [];
    if (this.state.loadedCurrentData.length === 0) {
      return null;
    }
    this.state.loadedCurrentData.forEach((itemData) => {
      tiles.push(<Tile {...itemData} imageSrc={imagesUrls[itemData.name]} key={itemData.name} />);
    });
    return tiles;
  }

  onLoad = (data) => {
    const { currentDayData } = this.props.baseReducer;
    this.setState(({ loadedCurrentData }) => {
      return { loadedCurrentData: loadedCurrentData.concat(data) };
    }, () => {
      const loading = this.state.loadedCurrentData.length !== currentDayData.length;
      this.setState({ loading });
    });
  }

  render() {
    const { imagesUrls, currentDayData } = this.props.baseReducer;
    return (
      <div className="items-grid">
      {this.itemGrid()}
        <div className="hidden">
          {currentDayData.map((itemData) => {
            return <img alt='' onLoad={() => this.onLoad(itemData)} src={imagesUrls[itemData.name]} key={itemData.name} />;
          })}
        </div>
        {this.state.loading && <Loader withText text="Loading food items..."/>}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

function mapDispatchToProps(dispatch) {
  const actions = bindActionCreators({ ...itemActions }, dispatch);
  return {
    actions
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemsGrid);
