import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import * as itemActions from "../actions";

class OrderTile extends Component {
  componentDidMount() {
    this.props.actions.updateQuantity({ id: this.props.id, quantity: 1 })
  }

  onClick = () => {
  }

  render() {
    const { imageSrc, id, name, price, quantity } = this.props;
    return (
      <div
        className={'order-tile'}
        >
        <img src={imageSrc} alt={name} />
        <span className="name">{name}</span>
        <span>&#8377; {price}</span>
        <div className="price">
          <button onClick={() => {
            if (quantity > 1) {
              this.props.actions.updateQuantity({ id, quantity: quantity - 1 })
            }
          }}>
            -
          </button>
          <span>{quantity}</span>
          <button onClick={() => {
            this.props.actions.updateQuantity({ id, quantity: quantity + 1 })
          }}>
            +
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

function mapDispatchToProps(dispatch) {
  const actions = bindActionCreators({ ...itemActions }, dispatch);
  return {
    actions
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderTile);
