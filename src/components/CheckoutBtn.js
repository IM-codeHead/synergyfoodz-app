import React, {Component} from 'react';
// import {init as firebaseInit} from '../javascripts/firebase';
// import configureStore from './configureStore';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import * as itemActions from "../actions";

class CheckoutBtn extends Component {
  onClick = () => {
    this.props.history.push('/order');
  }

  render() {
    const { currentDayData } = this.props.baseReducer;
    const disable = currentDayData.filter(obj => obj.checked === true).length === 0;
    return (
      <button
        className={`checkout-btn ${disable ? 'disable' : ''}`}
        onClick={this.onClick}
        >
        Checkout
      </button>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

function mapDispatchToProps(dispatch) {
  const actions = bindActionCreators({ ...itemActions }, dispatch);
  return {
    actions
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CheckoutBtn);
