import React, {Component} from 'react';
import CheckoutBtn from '../../components/CheckoutBtn';
import ItemsGrid from '../../components/ItemsGrid';
import './index.scss';

export default class CheckoutPage extends Component {
  render() {
    return (
      <React.Fragment>
        <ItemsGrid {...this.props}/>
        <CheckoutBtn {...this.props}/>
      </React.Fragment>
    );
  }
}
