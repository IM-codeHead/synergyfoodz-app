import React, {Component} from 'react';
import OrderTile from '../../components/OrderTile';
import PlaceOrder from '../../components/PlaceOrder';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import * as itemActions from "../../actions";
import './index.scss';

class OrderPage extends Component {
  render() {
    const { currentDayData, imagesUrls } = this.props.baseReducer;
    const currentData = currentDayData.filter(itemData => itemData.checked);
    const tiles = currentData.map(itemData => <OrderTile {...itemData} imageSrc={imagesUrls[itemData.name]} key={itemData.id}/>);

    const total = currentData.map(obj => obj.price * obj.quantity).reduce((acc, red) => {
      return acc + red;
    }, 0);

    return (
      <React.Fragment>
        <div className="tiles-holder">
          {tiles}
        </div>
        <div className="bottom">
          <div className="total">
            <span>Total amount to be paid</span>
            <span>{total}</span>
          </div>
          <PlaceOrder disable={total === 0} history={this.props.history}/>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

function mapDispatchToProps(dispatch) {
  const actions = bindActionCreators({ ...itemActions }, dispatch);
  return {
    actions
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderPage);
