import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import * as itemActions from "../../actions";
import PasswordField from '../../components/PasswordField';
import { DAYS } from "../../constants";
import { database, storage } from '../../firebase';
import './index.scss';

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this._password = '';
    this.state = { errorMsg: '' };
  }

  componentDidMount() {
    const { locations } = this.props.baseReducer;
    this.props.actions.setLocationData(locations[0].locationId);
  }

  onLocationSelect = e => {
    this.props.actions.setLocationData(e.currentTarget.value);
  }

  onPasswordChange = e => {
    this._password = e.currentTarget.value;
  }

  onSubmit = () => {
    const { password } = this.props.baseReducer.currentLocation;
    if (password === this._password) {
      this.setState({ errorMsg: '' });
      this.loadData();
      this.props.actions.setLoading(true);
      this.props.history.push('/checkoutPage');
    } else {
      this.setState({ errorMsg: 'Incorrect password' });
    }
  }

  loadData = () => {
    const { locationId } = this.props.baseReducer.currentLocation;
    const urls = {};
    database.ref(`/itemDetails/${locationId}`).on('value', snapshot => {
      const state = snapshot.val();
      const currentDayData = state[DAYS[new Date().getDay()]];
      const items = [];
      console.log('Updated database uploaded to store');
      currentDayData.forEach((itemIndex, index) => {
        const itemData = state.products.find(el => el.id === itemIndex)
        items.push(itemData);
        storage.child(`images/${itemData.imgSrc}`).getDownloadURL().then((url) => {
          urls[itemData.name] = url;
          if (index === currentDayData.length - 1) {
            this.props.actions.setUrls({ imagesUrls: urls });
            this.props.actions.setStore({ currentDayData: items.map(obj => { return { ...obj, checked: false, quantity: 0 } }) });
            this.props.actions.setLoading(false);
            console.log('All image urls retrieved');
          }
        }).catch(function(error) {
          console.log(error);
        });
      });
    });
  }

  render() {
    const { locations } = this.props.baseReducer;
    return (
      <div className="login-page">
        <select onChange={this.onLocationSelect}>
          {locations.map(location => <option value={location.locationId} key={location.locationId}>{location.locationName}</option>)}
        </select>
        <PasswordField onChange={this.onPasswordChange} />
        {this.state.errorMsg !== '' && <div className="error">{this.state.errorMsg}</div>}
        <button className="login-btn" type="" onClick={this.onSubmit}>
          Log In
        </button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

function mapDispatchToProps(dispatch) {
  const actions = bindActionCreators({ ...itemActions }, dispatch);
  return {
    actions
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
