export const SET_STORE = 'SET_STORE';
export const SET_URLS = 'SET_URLS';
export const SET_LOADING = 'SET_LOADING';
export const ADD_REMOVE_ITEM = 'ADD_REMOVE_ITEM';
export const CLEAR_SELECTIONS = 'CLEAR_SELECTIONS';
export const UPDATE_QUANTITY = 'UPDATE_QUANTITY';
export const SET_LOCATION_DATA = 'SET_LOCATION_DATA';
export const SET_LOCATIONS = 'SET_LOCATIONS';

export const DAYS = ['Monday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Friday'];
